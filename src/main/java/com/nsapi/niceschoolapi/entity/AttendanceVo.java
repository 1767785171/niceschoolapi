package com.nsapi.niceschoolapi.entity;

public class AttendanceVo {
    private String dateStr;
    private Long count;


    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
