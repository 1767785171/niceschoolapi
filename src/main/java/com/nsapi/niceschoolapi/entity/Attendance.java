package com.nsapi.niceschoolapi.entity;

import java.util.Date;

public class Attendance {

    /**
     * 考勤id
     */
    private Integer aid;

    /**
     * 学生id
     */
    private Integer sid;

    /**
     * 课程id
     */
    private Integer cid;

    /**
     * 班级id
     */
    private Integer classid;

    /**
     * type: 1--出勤  2--请假  3--旷课
     */
    private Integer type;

    /**
     * 是否删除
     */
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createTime;

    public Attendance() {
    }

    public Attendance(Integer aid, Integer sid, Integer cid, Integer classid, Integer type, Integer isDel, Date createTime) {
        this.aid = aid;
        this.sid = sid;
        this.cid = cid;
        this.classid = classid;
        this.type = type;
        this.isDel = isDel;
        this.createTime = createTime;
    }

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Attendance{" +
                "aid=" + aid +
                ", sid=" + sid +
                ", cid=" + cid +
                ", classid=" + classid +
                ", type=" + type +
                ", isDel=" + isDel +
                ", createTime=" + createTime +
                '}';
    }
}
