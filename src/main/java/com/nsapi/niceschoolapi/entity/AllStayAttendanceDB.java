package com.nsapi.niceschoolapi.entity;

public class AllStayAttendanceDB {

    /**
     * 学生id
     */
    private Integer sid;

    /**
     * 学生姓名
     */
    private String sname;

    /**
     * 课程id
     */
    private Integer cid;

    /**
     * 课程名称
     */
    private String cname;

    /**
     * 班级id
     */
    private Integer classid;

    /**
     * 班级名称
     */
    private String classname;

    public AllStayAttendanceDB() {
    }

    public AllStayAttendanceDB(Integer sid, String sname, Integer cid, String cname, Integer classid, String classname) {
        this.sid = sid;
        this.sname = sname;
        this.cid = cid;
        this.cname = cname;
        this.classid = classid;
        this.classname = classname;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public Integer getClassid() {
        return classid;
    }

    public void setClassid(Integer classid) {
        this.classid = classid;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    @Override
    public String toString() {
        return "AllStayAttendanceDB{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                ", cid=" + cid +
                ", cname='" + cname + '\'' +
                ", classid=" + classid +
                ", classname='" + classname + '\'' +
                '}';
    }
}
