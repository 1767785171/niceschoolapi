package com.nsapi.niceschoolapi.entity;

public class StayAttendanceVo {

    /**
     * 学号
     */
    private String stui;

    /**
     * 姓名
     */
    private String name;

    /**
     * 班级名称
     */
    private String classname;

}
