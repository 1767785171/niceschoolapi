package com.nsapi.niceschoolapi.service;

import com.nsapi.niceschoolapi.entity.AttendanceVo;
import com.nsapi.niceschoolapi.entity.StuExamVO;

import java.util.List;

public interface AttendanceService {
    List<AttendanceVo> getLineByAttendance();

    List<Integer> getBarStuExam();

}
