package com.nsapi.niceschoolapi.service.impl;

import com.nsapi.niceschoolapi.entity.AttendanceVo;
import com.nsapi.niceschoolapi.entity.StuExamVO;
import com.nsapi.niceschoolapi.mapper.AttendanceMapper;
import com.nsapi.niceschoolapi.mapper.StuExamMapper;
import com.nsapi.niceschoolapi.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class AttendanceServiceImpl implements AttendanceService {

    @Autowired
    private AttendanceMapper attendanceMapper;
    @Autowired
    private StuExamMapper stuExamMapper;

    @Override
    public List<AttendanceVo> getLineByAttendance() {
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        Date nowDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(nowDate);
        calendar.set(Calendar.DATE,calendar.get(Calendar.DATE) - 15);
        Date time = calendar.getTime();
        String endDate = sf.format(nowDate);
        String startDate = sf.format(time);
        System.out.println(startDate + "==========" + endDate);
        List <AttendanceVo> list =  attendanceMapper.getLineByAttendance(startDate,endDate);
        return list;
    }

    @Override
    public List<Integer> getBarStuExam() {
        List<StuExamVO> stuExamVOS =  stuExamMapper.getBarStuExam();

        ArrayList<Integer> list = new ArrayList<>();
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
        for (StuExamVO stuExamVO : stuExamVOS) {
            if (stuExamVO.getTestexam() < 60){
                a++;
            }
            if (stuExamVO.getTestexam() >= 60 && stuExamVO.getTestexam() <70){
                b ++;
            }
            if (stuExamVO.getTestexam() >= 70 && stuExamVO.getTestexam() <80){
                c ++;
            }
            if (stuExamVO.getTestexam() >= 80 && stuExamVO.getTestexam() <90){
                d ++;
            }
            if (stuExamVO.getTestexam() >= 90 && stuExamVO.getTestexam() <101){
                e ++;
            }
        }
        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(e);

        return list;
    }
}
