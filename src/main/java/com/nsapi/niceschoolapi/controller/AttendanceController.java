package com.nsapi.niceschoolapi.controller;

import com.nsapi.niceschoolapi.common.config.MySysUser;
import com.nsapi.niceschoolapi.common.util.DateUtil;
import com.nsapi.niceschoolapi.entity.*;
import com.nsapi.niceschoolapi.mapper.AttendanceMapper;
import com.nsapi.niceschoolapi.mapper.StudentMapper;
import com.nsapi.niceschoolapi.service.AttendanceService;
import com.nsapi.niceschoolapi.service.TchCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Controller
public class AttendanceController {

    @Autowired
    private TchCourseService tchCourseService;

    @Resource
    private StudentMapper studentMapper;

    @Resource
    private AttendanceMapper attendanceMapper;

    @Autowired
    private AttendanceService attendanceService;

    @RequestMapping("/getCurriculum")
    public String getCurriculum(Model model) {
        String tid = MySysUser.loginName();
        TchCourseVO tchCourseVO = new TchCourseVO();
        tchCourseVO.setTid(Integer.valueOf(tid));
        List<TchCourseVO> courseByTch = tchCourseService.findCourseByTch(tchCourseVO);
        model.addAttribute("courseByTch", courseByTch);
        return "view/student/AllStayAttendance";
    }

    @RequestMapping("/getAllStayAttendance")
    @ResponseBody
    public LayuiResult<AllStayAttendanceDB> getAllStayAttendance(Integer cid) {
        List<AllStayAttendanceDB> allStayAttendanceDBS = studentMapper.selectKe(cid);
        LayuiResult<AllStayAttendanceDB> result = new LayuiResult<>();
        result.setData(allStayAttendanceDBS);
        return result;
    }

    @RequestMapping("/entryAttendance")
    @ResponseBody
    public String entryAttendance(Integer type, Integer cid, Integer classid, Integer sid) { // type: 1--出勤  2--请假  3--旷课
        System.out.println("--------------|" + type + "|" + cid + "|" + classid + "|" + sid + "|");
        Date dayBegin = DateUtil.getDayBegin();
        Date dayEnd = DateUtil.getDayEnd();
        Attendance attendance = new Attendance();
        attendance.setSid(sid);
        attendance.setCid(cid);
        attendance.setClassid(classid);
        Integer whetherAttendance = attendanceMapper.whetherAttendance(attendance, dayBegin, dayEnd);
        if (Objects.nonNull(whetherAttendance) && whetherAttendance > 0) {
            return "该学员已录入考勤，请忽重复录入";
        }
        // 执行录入
        attendance.setType(type);
        int save = attendanceMapper.save(attendance);
        String ret = type == 1 ? "出勤" : type == 2 ? "请假" : "旷课";
        ret =  ret + (save > 0 ? "成功" : "失败");
        return "录入" + ret;
    }

    //进入考勤统计页面
    @GetMapping("/attendanceList")
    public String getAttendanceList(){
        return "view/attendance/list";
    }

    //进入成绩统计页面
    @GetMapping("/stuExamList")
    public String stuExamList(){
        return "view/student/studentExam";
    }

    //考勤折线图
    @RequestMapping("/getLineByAttendance")
    @ResponseBody
    public LayuiResult<AttendanceVo> getLineByAttendance(){
        List<AttendanceVo> list = attendanceService.getLineByAttendance();
        LayuiResult<AttendanceVo> result = new LayuiResult<>();
        result.setData(list);
        return result;
    }
    //成绩柱状图
    @GetMapping("/getBarStuExam")
    @ResponseBody
    public LayuiResult<Integer> getBarStuExam(){
        List<Integer> list = attendanceService.getBarStuExam();
        LayuiResult<Integer> result = new LayuiResult<>();
        result.setData(list);
        return result;
    }
}
