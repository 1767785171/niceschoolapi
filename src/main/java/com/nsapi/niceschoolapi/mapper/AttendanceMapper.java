package com.nsapi.niceschoolapi.mapper;

import com.nsapi.niceschoolapi.entity.Attendance;
import com.nsapi.niceschoolapi.entity.AttendanceVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface AttendanceMapper {
    Integer whetherAttendance(@Param("attendance") Attendance attendance,
                              @Param("startDate") Date startDate,
                              @Param("endDate") Date endDate);

    int save(Attendance attendance);

    List<AttendanceVo> getLineByAttendance(@Param("startDate") String startDate, @Param("endDate") String endDate);
}
